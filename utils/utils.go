package utils

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"syscall"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

const (
	// FlagJavaPath is the name of spotbug's cli java path argument
	FlagJavaPath = "javaPath"
	// FlagJavaVersion is the name of spotbug's cli java version argument
	FlagJavaVersion = "javaVersion"
	// FlagJava8Version is the name of spotbug's cli java 8 version argument
	FlagJava8Version = "java8Version"
	// FlagJava11Version is the name of spotbug's cli java 11 version argument
	FlagJava11Version = "java11Version"
)

// Setup involves configuration of a environment for installation prerequisites
type Setup struct {
	cmd *exec.Cmd
}

// Run executes the Setup command, logs output, and returns potential errors
func (setup *Setup) Run() error {
	if setup.cmd == nil {
		return nil
	}

	output, err := setup.cmd.CombinedOutput()

	log.Debugf("%s\n%s", setup.cmd.String(), output)

	if err != nil {
		log.Warnf("Failed to set system Java: %s\n", err.Error())

		return err
	}
	return nil
}

// RunCmdError are errors for cmd runs that include an exit code
type RunCmdError struct {
	err      string
	exitCode int
}

// NewRunCmdError returns a new run command error
func NewRunCmdError(exitCode int, message string) error {
	return &RunCmdError{
		err:      message,
		exitCode: exitCode,
	}
}

func (e *RunCmdError) Error() string {
	return e.err
}

// RunCmd runs a command and gets the exit code.
func RunCmd(cmd *exec.Cmd) error {
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)

	if err != nil {
		switch err.(type) {
		case *exec.ExitError:
			// The command failed during its execution
			exitError := err.(*exec.ExitError)
			waitStatus := exitError.Sys().(syscall.WaitStatus)
			return NewRunCmdError(waitStatus.ExitStatus(), err.Error())
		default:
			// The command couldn't even be executed
			return NewRunCmdError(1, fmt.Sprintf("Command couldn't be executed: %v", err))
		}
	}

	waitStatus := cmd.ProcessState.Sys().(syscall.WaitStatus)
	exitStatus := waitStatus.ExitStatus()

	if exitStatus != 0 {
		return NewRunCmdError(exitStatus, "Command returned a non zero exit status")
	}

	return nil
}

// RunCmdWithTextErrorDetection runs a command and returns an error code according to the presence
// of a string in the output.
// Uses code from https://github.com/kjk/go-cookbook in the public domain
func RunCmdWithTextErrorDetection(cmd *exec.Cmd, c *cli.Context, errorText string, message string) error {
	output, err := cmd.CombinedOutput()
	if err != nil {
		return err
	}

	// Print output
	log.Debugf("%s\n%s", cmd.String(), output)

	// Detect error string
	if strings.Index(string(output), errorText) != -1 {
		// Error text is present, return error.
		return errors.New(message)
	}

	return nil
}

// SetupCmdNoStd sets up a command's directory and environment for execution.
func SetupCmdNoStd(projectPath string, cmd *exec.Cmd) *exec.Cmd {
	cmd.Dir = projectPath
	cmd.Env = os.Environ()
	return cmd
}

// WithWarning runs the function passed as argument and prints a warning if it returns an error
func WithWarning(warning string, fun func() error) {
	err := fun()
	if err != nil {
		log.Warnf("%s (%s)\n", warning, err.Error())
	}
}

// SetupSystemJava sets up the system so that SpotBugs and it's dependencies (e.g. Maven) use the same Java
func SetupSystemJava(c *cli.Context) *Setup {
	if usesCustomJavaPath(c) {
		return &Setup{}
	}

	javaVersion := selectedSystemJava(c)

	return &Setup{
		cmd: exec.Command(
			"/bin/bash",
			"-c",
			fmt.Sprintf("source /root/.bashrc"+
				" && switch_to java %s", javaVersion)),
	}
}

// determine the version of the selected Java to use
func selectedSystemJava(c *cli.Context) string {
	switch c.String(FlagJavaVersion) {
	case "11":
		return c.String(FlagJava11Version)
	case "8":
		return c.String(FlagJava8Version)
	default:
		log.Warnf(
			"Java version %s is not supported. Valid values are 8, 11. Using Java 8.\n",
			c.String(FlagJavaVersion))
		return c.String(FlagJava8Version)
	}
}

// JavaPath determines the path to the java executable
func JavaPath(c *cli.Context) string {
	if usesCustomJavaPath(c) {
		return c.String(FlagJavaPath)
	}

	return "java"
}

// returns based on whether or not using a custom Java
func usesCustomJavaPath(c *cli.Context) bool {
	return c.String(FlagJavaPath) != "java" && c.String(FlagJavaPath) != ""
}
